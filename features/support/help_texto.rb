class HelpTexto
    def self.remover_texto_apos_marcadores(string_de_entrada, marcadores)
      marcadores.each do |marcador|
        if string_de_entrada.include?(marcador)
          string_de_entrada = string_de_entrada.split(marcador).first.strip
        end
      end
      string_de_entrada
    end
end
  