require_relative '../support/help_texto'

Given('a string de entrada {string}') do |string_de_entrada|
  @string_de_entrada = string_de_entrada
end

Given('os marcadores {string}') do |marcadores|
  @marcadores = marcadores.split(', ').map(&:strip)
end

Then('a saída esperada é {string}') do |saida_esperada|
  saida_real = HelpTexto.remover_texto_apos_marcadores(@string_de_entrada, @marcadores)
  expect(saida_real).to eq(saida_esperada)
end
