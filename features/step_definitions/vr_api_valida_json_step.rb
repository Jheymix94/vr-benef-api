require 'httparty'
require 'json'

Given('que eu realizo uma requisição GET para o endpoint') do
  @response = HTTParty.get('https://portal.vr.com.br/api-web/comum/enumerations/VRPAT')
end

Then('o JSON deve conter a chave {string}') do |key|
  json_response = JSON.parse(@response.body)
  expect(json_response).to have_key(key)
end

And('é exibido o tipo de estabelecimento informado da resposta') do
  json_response = JSON.parse(@response.body)
  tipo_estabelecimento = json_response['typeOfEstablishment']
  estabelecimento_resposta = tipo_estabelecimento.sample
  puts "Tipo de Estabelecimento: #{estabelecimento_resposta}"
end
