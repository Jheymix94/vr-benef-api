Feature: Teste de API com HTTParty e Cucumber

  Scenario: Validar o JSON e exibir o tipo de estabelecimento informado
    Given que eu realizo uma requisição GET para o endpoint
    Then o JSON deve conter a chave "typeOfEstablishment"
    And é exibido o tipo de estabelecimento informado da resposta
