# Projeto de Automação de Testes de API com Cucumber, Ruby e HTTParty

Projeto de testes automatizados para validar a API do portal VR, e a funcionalidade de removacao de texto com marcadores. 
Os testes são escritos em Ruby usando Cucumber e HTTParty.

## Estrutura do Projeto
```
vr_benef_api/
├── Gemfile
├── Gemfile.lock
├── cucumber.yml
├── features/
│ ├── api/
│ │ └── vr_api_valida_json_page.rb
│ ├── step_definitions/
│ │ ├── vr_api_valida_json_step.rb
│ │ └── vr_frutas_step.rb
│ ├── support/
│ │ ├── env.rb
│ │ └── help_texto.rb
│ ├── vr_api_valida_json.feature
│ └── vr_frutas.feature
```

## Configuração

### Pré-requisitos

- [Ruby](https://www.ruby-lang.org/en/documentation/installation/) (versão 2.7 ou superior)
- [Bundler](https://bundler.io/)

### Instalação

1. Clone o repositório:

```
git clone <url-do-repositorio>
cd vr_benef_api
```


2. Instale as dependências:

```
bundle install
```

## Executando os Testes

Testes de API

### Para executar os testes de API:
```
bundle exec cucumber features/vr_api_valida_json.feature
```

### Para executar os testes de remocão de texto:
```
bundle exec cucumber features/vr_frutas.feature
```

### Para executar todos os testes:
```
bundle exec cucumber
```
